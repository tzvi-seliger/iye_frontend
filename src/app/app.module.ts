import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import {AuthService } from './services/auth.service';
import { AccountsService } from './services/accounts.service';
import { DataService } from './services/MyData.service';
import {TrainingsService} from './services/trainings.service';
import {ArticlesService} from './services/articles.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared-components/header/header.component';
import { FooterComponent } from './components/shared-components/footer/footer.component';
import { BodyComponent } from './components/body/body.component';
import { HeaderSectionComponent } from './components/shared-components/header/header-section/header-section.component';
import { BodySectionComponent } from './components/body/body-section/body-section.component';
import { FooterSectionComponent } from './components/shared-components/footer/footer-section/footer-section.component';
import { AboutComponent } from './components/about/about.component';
import { AccountInfoComponent } from './components/account-info/account-info.component';
import { PricingInfoComponent } from './components/pricing-info/pricing-info.component';
import { RegisterButtonComponent } from './components/registerbutton/register-button.component';
import { TrainingsComponent } from './components/trainings/trainings.component';
import { ProfileEmployeeComponent } from './components/profile-employee/profile-employee.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { TrainingComponent } from './components/training/training.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { BlogComponent } from './components/blog/blog.component';
import { BlogPostComponent } from './components/blog/blog-post/blog-post.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule, matDatepickerAnimations } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatNativeDateModule } from '@angular/material';
import { ArticleComponent } from './components/article/article.component';
import { ArticlesComponent } from './components/articles/articles.component';
import { ArticleScrollComponent } from './components/articles/article-scroll/article-scroll.component';
import { ArticleFormComponent } from './components/articles/article-form/article-form.component';
import { DefTextInputComponent } from './components/shared-components/def-text-input/def-text-input.component';
import { DefLabelComponent } from './components/shared-components/def-label/def-label.component';
import { DefButtonComponent } from './components/shared-components/def-button/def-button.component';
import { AccountManagementComponent } from './components/account-management/account-management.component';
import { GeneralComponent } from './components/general/general.component';
import { LocalStorageService } from './services/storage.service';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { BlogPostFullComponent } from './components/blog-post-full/blog-post-full.component';
import { TermListComponent } from './components/term-list/term-list.component';
import { TermListItemComponent } from './components/term-list-item/term-list-item.component';
import { ProfileBusinessComponent } from './components/profile-business/profile-business.component';
// tslint:disable-next-line:max-line-length
import { ArticleScrollItemDropdownComponent } from './components/articles/article-scroll/article-scroll-item-dropdown/article-scroll-item-dropdown.component';
import { FindIdiotComponent } from './components/pages/find-idiot/find-idiot.component';
import { CallScreenComponent } from './components/pages/call-screen/call-screen.component';
import { IdiotProfileComponent } from './components/pages/idiot-profile/idiot-profile.component';
import { FindPositionComponent } from './components/pages/find-position/find-position.component';
import { BillingComponent } from './components/pages/billing/billing.component';
import { TrainingCreationComponent } from './components/pages/training-creation/training-creation.component';
import { AddPositionComponent } from './components/pages/add-position/add-position.component';
import { HierarchyComponent } from './components/pages/hierarchy/hierarchy.component';
import { BusinessProfileComponent } from './components/pages/business-profile/business-profile.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { SkillsComponent } from './components/pages/skills/skills.component';
import { InterestsComponent } from './components/pages/interests/interests.component';
import { ExperienceComponent } from './components/pages/experience/experience.component';
import { PortfolioComponent } from './components/pages/portfolio/portfolio.component';
import { AuthoredTrainingsComponent } from './components/pages/authored-trainings/authored-trainings.component';
import { CompletedTrainingsComponent } from './components/pages/completed-trainings/completed-trainings.component';
import { BrowseTrainingsComponent } from './components/pages/browse-trainings/browse-trainings.component';
import { PositionComponent } from './components/pages/position/position.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    HeaderSectionComponent,
    BodySectionComponent,
    FooterSectionComponent,
    AboutComponent,
    AccountInfoComponent,
    PricingInfoComponent,
    RegisterButtonComponent,
    TrainingsComponent,
    ProfileEmployeeComponent,
    EmployeesComponent,
    TrainingComponent,
    LandingPageComponent,
    RegistrationFormComponent,
    BlogComponent,
    BlogPostComponent,
    ArticleComponent,
    ArticlesComponent,
    ArticleScrollComponent,
    ArticleFormComponent,
    DefTextInputComponent,
    DefLabelComponent,
    DefButtonComponent,
    AccountManagementComponent,
    GeneralComponent,
    SignupFormComponent,
    BlogPostFullComponent,
    TermListComponent,
    TermListItemComponent,
    ArticleScrollItemDropdownComponent,
    FindIdiotComponent,
    CallScreenComponent,
    IdiotProfileComponent,
    FindPositionComponent,
    BusinessProfileComponent,
    BillingComponent,
    TrainingCreationComponent,
    AddPositionComponent,
    HierarchyComponent,
    BusinessProfileComponent,
    LandingComponent,
    SkillsComponent,
    InterestsComponent,
    ExperienceComponent,
    PortfolioComponent,
    AuthoredTrainingsComponent,
    CompletedTrainingsComponent,
    BrowseTrainingsComponent,
    PositionComponent
  ],
  imports: [
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: 'landing', component: LandingComponent },
      { path: 'idiot-profile', component: IdiotProfileComponent },
      { path: 'interests', component: InterestsComponent },
      { path: 'skills', component: SkillsComponent },
      { path: 'experience', component: ExperienceComponent },
      { path: 'portfolio', component: PortfolioComponent },
      { path: 'completed-trainings', component: CompletedTrainingsComponent },
      { path: 'authored-trainings', component: AuthoredTrainingsComponent },
      { path: 'browse-trainings', component: BrowseTrainingsComponent },
      { path: 'find-position', component: FindPositionComponent },
      { path: 'find-idiot', component: FindIdiotComponent },
      { path: 'call', component: CallScreenComponent },
      { path: 'position', component: PositionComponent },
      { path: 'business-profile', component: BusinessProfileComponent },
      { path: 'billing', component: BillingComponent },
      { path: 'create-training', component: TrainingCreationComponent },
      { path: 'add-position', component: AddPositionComponent },
      { path: 'hierarchy', component: HierarchyComponent },

/*
      { path: 'landing', component: LandingPageComponent },
      { path: '', redirectTo: 'landing', pathMatch: 'full'},
      { path: 'dashboard', component: DashboardComponent },
      { path: 'employee/:employeeId', component: ProfileEmployeeComponent },
      { path: 'business/:employeeId', component: ProfileBusinessComponent },
      { path: 'employees', component: EmployeesComponent },
      { path: 'blog', component: BlogComponent },
      { path: 'blog-item', component: BlogPostFullComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'about', component: AboutComponent },
      { path: 'contact', component: AboutComponent },
      { path: 'accountinfo', component: AccountInfoComponent },
      { path: 'pricing', component: PricingInfoComponent },
      { path: 'random', component: AboutComponent },
      { path: 'trainings', component: TrainingsComponent },
      { path: 'training/:trainingId', component: TrainingComponent },
      { path: 'articles/:articleId', component: ArticlesComponent },
      { path: 'articles', component: ArticlesComponent },
      { path: 'general', component: GeneralComponent },
      { path: 'register', component: SignupFormComponent },
      { path: 'terms', component: TermListComponent },
      { path: 'term-item', component: TermListItemComponent }
      */
    ]),
    CommonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    AccountsService,
    DataService,
    AuthService,
    ArticlesService,
    LocalStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
