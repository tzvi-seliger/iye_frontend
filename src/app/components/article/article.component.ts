import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from '../../models/Article';
import { ArticleBase } from '../../models/ArticleBase';
import { ArticlesService } from '../../services/articles.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  currentArticle: Article;
  editedArticle: Article;
  subscription: any;
  params;
  employeeName;
  id;
  isInEditMode = false;
  articles: ArticleBase[] = [];
  articleId: number;
  @Output() articleSelected = new EventEmitter<number>();

  constructor(private articleService: ArticlesService,
              private route: ActivatedRoute,
              ) { }

  ngOnInit(): void {
    this.getArticle();
    this.articleService.getArticleTitles().subscribe((res) => {
      console.log(res);
      this.articles = res;
    });
  }

  getArticle() {
    this.subscription = this.route.paramMap.subscribe(params => {
      this.id = parseInt(params.get('articleId'), 10);
      this.articleService.getArticleById(this.id).subscribe((res) => {
        this.currentArticle = res;
        console.log(res);
      });
    });
  }

  toggleEditMode() {
    this.isInEditMode = !this.isInEditMode;
  }

  editArticle(title: string, description: string, content: string, author: string, rating: string, subject: string, level: string) {
    this.editedArticle = {
      title,
      description,
      content,
      id: this.id,
      author,
      rating: parseInt(rating, 10),
      subject,
      level
    };

    this.articleService.editArticle(this.editedArticle).subscribe((res) => this.getArticle());

    console.log(this.editedArticle);

    this.toggleEditMode();
  }

  updateField(prop: string, val: string) {
    this.editedArticle[prop] = val;
  }

    setId(val: number) {
      this.articleSelected.emit(val);
    }
}
