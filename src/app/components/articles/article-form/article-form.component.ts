import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Article } from '../../../models/Article';
import { ArticlePost } from '../../../models/ArticlePost';
import { ArticlesService } from '../../../services/articles.service';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss']
})

export class ArticleFormComponent implements OnInit {

  titleLabel = 'Title';
  descriptionLabel = 'Description';
  contentLabel = 'Content';
  ratingLabel = 'Rating';
  subjectLabel = 'Subject';
  authorLabel = 'Author';
  levelLabel = 'Level';
  buttonText = 'Save';
  article: ArticlePost;

  articleForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
    content: new FormControl(''),
    rating: new FormControl(''),
    subject: new FormControl(''),
    author: new FormControl(''),
    level: new FormControl('')
  });

  constructor(private articleService: ArticlesService) { }

  ngOnInit(): void {
  }

  submitArticle() {

    this.article = {
      content: this.articleForm.value.content,
      title: this.articleForm.value.title,
      description: this.articleForm.value.description,
      rating: parseInt(this.articleForm.value.rating, 10),
      subject: this.articleForm.value.subject,
      author: this.articleForm.value.author,
      level: this.articleForm.value.level
    };
    console.log(this.article);
    this.articleService.addArticle(this.article).subscribe();
  }

}
