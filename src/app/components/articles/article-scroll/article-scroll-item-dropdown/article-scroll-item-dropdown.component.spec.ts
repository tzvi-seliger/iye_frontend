import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleScrollItemDropdownComponent } from './article-scroll-item-dropdown.component';

describe('ArticleScrollItemDropdownComponent', () => {
  let component: ArticleScrollItemDropdownComponent;
  let fixture: ComponentFixture<ArticleScrollItemDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleScrollItemDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleScrollItemDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
