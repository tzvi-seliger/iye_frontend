import { Component, OnInit } from '@angular/core';
import { ArticleBase } from '../../../models/ArticleBase';
import { ArticlesService } from '../../../services/articles.service';
import { Output, EventEmitter } from '@angular/core';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-article-scroll',
  templateUrl: './article-scroll.component.html',
  styleUrls: ['./article-scroll.component.scss']
})
export class ArticleScrollComponent implements OnInit {
  articles: ArticleBase[] = [];
  articleId: number;
  areFiltersVisible = false;
  authors: string[];
  levels: string[];
  ratings: string[];
  subjects: string[];
  displayDelete = false;
  filters = {
    Authors: []
  };
  dropdownActive = false;
  activeHamburger = -1;

  filterByAuthor = false;

  constructor(private articleService: ArticlesService) { }

  @Output() articleSelected = new EventEmitter<number>();

  ngOnInit() {
    this.articleService.getArticleTitles().subscribe((res) => {
      console.log(res);
      this.articles = res;
    });
    this.articleService.getAuthors().subscribe((res) => {
      console.log(res);
      this.authors = res;
    });
    this.articleService.getLevels().subscribe((res) => {
      console.log(res);
      this.levels = res;
    });
    this.articleService.getRatings().subscribe((res) => {
      console.log(res);
      this.ratings = res;
    });
    this.articleService.getSubjects().subscribe((res) => {
      console.log(res);
      this.subjects = res;
    });
  }

  toggleArticleItemDropdown(id) {
    this.activeHamburger = id;
    this.dropdownActive = !this.dropdownActive;
    console.log(this.dropdownActive);
  }

  toggleFilterByAuthor() {
    this.filterByAuthor = !this.filterByAuthor;
  }

  onAuthorChange(e) {
    if (e.target.checked) {
      this.filters.Authors.push(e.target.value);
    }

    if (!e.target.checked) {
      const index = this.filters.Authors.indexOf(e.target.value);
      if (index > -1) {
        this.filters.Authors.splice(index, 1);
      }
    }

    this.articleService.getArticleTitlesFiltered(this.filters).subscribe((res) => {
      this.articles = res;
    });
  }

  showFilters() {
    this.areFiltersVisible = !this.areFiltersVisible;
  }

  deleteArticle(id: string) {
    this.articleService.deleteArticle(id).subscribe(res => this.articleService.getArticleTitles().subscribe((res2) => {
      this.articles = res2;
    }));
  }
  
  setId(val: number) {
    this.articleSelected.emit(val);
  }
}
