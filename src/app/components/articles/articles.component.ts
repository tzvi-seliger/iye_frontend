import { Component, OnInit } from '@angular/core';
import { StorageService } from 'ngx-webstorage-service';
import { LocalStorageService } from '../../services/storage.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})

export class ArticlesComponent implements OnInit {
  articleEdit = false;
  myId: number;
  displayRightColumn = false;
  displayLeftColumn = false;

  constructor(private storage: LocalStorageService) { }
  ngOnInit(): void {}

  getId(item: number) {
    this.myId = item;
    console.log(this.myId);
  }

  toggleEdit() {
    this.articleEdit =  !this.articleEdit;
    console.log(this.storage.retrieveUserId());
  }

  toggleDisplayRight() {
    this.displayRightColumn = !this.displayRightColumn;
  }

  toggleDisplayLeft() {
    this.displayLeftColumn = !this.displayLeftColumn;
  }

}
