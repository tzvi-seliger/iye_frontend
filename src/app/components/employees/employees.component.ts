import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
  employeeName;
  companySkills;
  companyTrainings;
  companyPositions;
  employees: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.employeeName = 'Tzvi Seliger';
    this.userService.getUsers().subscribe(res => {
      console.log(res);
      this.employees = res;
    });
  }

}
