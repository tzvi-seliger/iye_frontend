import { Component, OnInit, Output } from '@angular/core';
import { EmployeeTraining } from '../../models/EmployeeTraining';
import { User } from '../../models/User';
import { LocalStorageService } from '../../services/storage.service';
import { TrainingsService } from '../../services/trainings.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})

export class GeneralComponent implements OnInit {
trainings: EmployeeTraining[];
userId = this.storageService.retrieveUserId();
user: User;

  constructor(private storageService: LocalStorageService,
              private userService: UserService,
              private trainingService: TrainingsService) { }

  ngOnInit(): void {
    // this.userService.getUserById(this.userId).subscribe(user => this.user =  user);
    // console.log(this.userId);
    // this.trainingService.getEmployeeTrainingsById(this.userId).subscribe(trainings => this.trainings = trainings);
  }

}
