import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthoredTrainingsComponent } from './authored-trainings.component';

describe('AuthoredTrainingsComponent', () => {
  let component: AuthoredTrainingsComponent;
  let fixture: ComponentFixture<AuthoredTrainingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthoredTrainingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthoredTrainingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
