import { Component, OnInit } from '@angular/core';
import { UserTrainingsService } from 'src/app/services/Users/trainings.service';
import {Training } from 'src/app/models/Training';

@Component({
  selector: 'app-authored-trainings',
  templateUrl: './authored-trainings.component.html',
  styleUrls: ['./authored-trainings.component.scss']
})
export class AuthoredTrainingsComponent implements OnInit {

  constructor(private userTrainingsService: UserTrainingsService) { }

  showAuthoredTrainings = false;
  authoredTrainings: Training[];
  showDevNotes = false;

  ngOnInit(): void {
    this.userTrainingsService.GetUserAuthoredTrainings(7).subscribe((res) => {
      console.log(res);
      this.authoredTrainings = res;
    });
  }

  toggleTrainings() {
    this.showAuthoredTrainings = !this.showAuthoredTrainings;
  }

}
