import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseTrainingsComponent } from './browse-trainings.component';

describe('BrowseTrainingsComponent', () => {
  let component: BrowseTrainingsComponent;
  let fixture: ComponentFixture<BrowseTrainingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseTrainingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseTrainingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
