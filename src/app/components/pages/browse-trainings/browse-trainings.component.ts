import { Component, OnInit } from '@angular/core';
import { ArticleBase } from 'src/app/models/ArticleBase';
import { Training } from 'src/app/models/Training';
import { ArticlesService } from 'src/app/services/articles.service';
import { TrainingsService } from 'src/app/services/trainings.service';


@Component({
  selector: 'app-browse-trainings',
  templateUrl: './browse-trainings.component.html',
  styleUrls: ['./browse-trainings.component.scss']
})
export class BrowseTrainingsComponent implements OnInit {
  articles: ArticleBase[] = [];
  trainings: Training[];


  constructor(
          private articlesService: ArticlesService,
          private trainingsService: TrainingsService
        ) { }

  ngOnInit(): void {
    this.articlesService.getArticleTitles().subscribe((res) => {
      console.log(res);
      this.articles = res;
    });

    this.trainingsService.getTrainings().subscribe((res) => {
      console.log(res);
      this.trainings = res;
    });
  }

}
