import { Component, OnInit } from '@angular/core';
import { UserTrainingsService } from 'src/app/services/Users/trainings.service';
import {Training } from 'src/app/models/Training';


@Component({
  selector: 'app-completed-trainings',
  templateUrl: './completed-trainings.component.html',
  styleUrls: ['./completed-trainings.component.scss']
})
export class CompletedTrainingsComponent implements OnInit {

  constructor(private userTrainingsService: UserTrainingsService) { }

  showCompletedTrainings = false;
  completedTrainings: Training[];
  showDevNotes = false;

  ngOnInit(): void {
    this.userTrainingsService.GetUserCompletedTrainings(7).subscribe((res) => {
      console.log(res);
      this.completedTrainings = res;
    });
  }

  toggleTrainings() {
    this.showCompletedTrainings = !this.showCompletedTrainings;
  }
}
