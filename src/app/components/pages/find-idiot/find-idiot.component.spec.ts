import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindIdiotComponent } from './find-idiot.component';

describe('FindIdiotComponent', () => {
  let component: FindIdiotComponent;
  let fixture: ComponentFixture<FindIdiotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindIdiotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindIdiotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
