import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-find-idiot',
  templateUrl: './find-idiot.component.html',
  styleUrls: ['./find-idiot.component.scss']
})
export class FindIdiotComponent implements OnInit {
  users: User[];
  constructor(private usersService: UserService) { }

  ngOnInit(): void {
    this.usersService.getUsers().subscribe(res => {
      this.users = res;
      console.log(res);
    });
  }
}
