import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindPositionComponent } from './find-position.component';

describe('FindPositionComponent', () => {
  let component: FindPositionComponent;
  let fixture: ComponentFixture<FindPositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindPositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
