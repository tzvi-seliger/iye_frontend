import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdiotProfileComponent } from './idiot-profile.component';

describe('IdiotProfileComponent', () => {
  let component: IdiotProfileComponent;
  let fixture: ComponentFixture<IdiotProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdiotProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdiotProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
