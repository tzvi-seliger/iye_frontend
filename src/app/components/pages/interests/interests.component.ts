import { Component, OnInit } from '@angular/core';
import { InterestsService } from '../../../services/Users/interests.service';
import { Interest } from '../../../models/Users/Interest';
import { LocalStorageService } from 'src/app/services/storage.service';


@Component({
  selector: 'app-interests',
  templateUrl: './interests.component.html',
  styleUrls: ['./interests.component.scss']
})

export class InterestsComponent implements OnInit {
  userId: number;

  showInterests = false;
  userInterests: Interest[];
  showDevNotes = false;

  interestToAdd: Interest;
  interestToUpdate: Interest;

  constructor(
     private interestsService: InterestsService,
     private storageService: LocalStorageService
     ) { }

  ngOnInit(): void {
    this.interestsService.GetInterests(1).subscribe((res) => {
      console.log(res);
      this.userInterests = res;
    });

    this.userId = this.storageService.retrieveUserId();
    console.log(this,userId;)
  }

  toggleInterests() {
    this.showInterests = !this.showInterests;
  }

  toggleDevNotes() {
    this.showDevNotes = !this.showDevNotes;
  }

  addInterest() {
    this.interestToAdd = {
      userId: 1,
      interest: 'Biology',
      description: 'Terms and definitions of study of living things'
    };

    this.interestsService.addInterest(this.interestToAdd);
  }

  editInterest() {
    this.interestToUpdate = {
      userId: 1,
      interest: 'Computer Science',
      description: 'The study of computers'
    };

    this.interestsService.editInterest('1', this.interestToUpdate);
  }

  deleteInterest() {
    this.interestsService.deleteInterest('1');
  }

}
