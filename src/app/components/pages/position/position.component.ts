import { Position } from 'src/app/models/Position';
import { Component, OnInit } from '@angular/core';
import { PositionsService } from 'src/app/services/positions.service';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss']
})
export class PositionComponent implements OnInit {

  showPositions = false;
  positions: Position[];
  showDevNotes = false;

  constructor(private positionsService: PositionsService) { }

  ngOnInit(): void {
    this.positionsService.getPositions().subscribe((res) => {
      this.positions = res;
    });
  }

  togglePositions() {
    this.showPositions = !this.showPositions;
  }

}
