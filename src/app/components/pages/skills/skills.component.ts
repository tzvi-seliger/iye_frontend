import { Component, OnInit } from '@angular/core';
import { SkillsService } from '../../../services/Users/skills.service';
import { Skill } from '../../../models/Users/Skills';


@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})

export class SkillsComponent implements OnInit {
  showSkills = false;
  userSkills: Skill[];
  showDevNotes = false;

  skillToAdd: Skill;
  skillToUpdate: Skill;

  constructor(private skillsService: SkillsService) { }

  ngOnInit(): void {
    this.skillsService.GetSkills(1).subscribe((res) => {
      console.log(res);
      this.userSkills = res;
    });
  }

  toggleSkills() {
    this.showSkills = !this.showSkills;
  }

  toggleDevNotes() {
    this.showDevNotes = !this.showDevNotes;
  }

  addSkill() {
    this.skillToAdd = {
      userId: 1,
      skill: 'Biology',
      description: 'Terms and definitions of study of living things'
    };

    this.skillsService.addSkill(this.skillToAdd);
  }

  editSkill() {
    this.skillToUpdate = {
      userId: 1,
      skill: 'Computer Science',
      description: 'The study of computers'
    };

    this.skillsService.editSkill('1', this.skillToUpdate);
  }

  deleteSkill() {
    this.skillsService.deleteSkill('1');
  }

}
