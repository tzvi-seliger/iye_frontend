import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingCreationComponent } from './training-creation.component';

describe('TrainingCreationComponent', () => {
  let component: TrainingCreationComponent;
  let fixture: ComponentFixture<TrainingCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
