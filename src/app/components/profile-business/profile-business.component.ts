import { Component, Input, OnInit } from '@angular/core';
import { TrainingsService } from '../../services/trainings.service';
import { EmployeeTraining } from '../../models/EmployeeTraining';
import { PostUserTraining } from '../../models/PostUserTraining';
import { User } from '../../models/User';
import { UserService } from '../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { UserExperience } from '../../models/UserExperience';

// https://stackoverflow.com/questions/63548416/fetching-data-from-multiple-service-on-single-component-using-rxjs-in-angular

@Component({
  selector: 'app-profile-business',
  templateUrl: './profile-business.component.html',
  styleUrls: ['./profile-business.component.scss']
})

export class ProfileBusinessComponent implements OnInit {
  user: User;
  id: number;

  constructor(private trainings: TrainingsService,
              private userService: UserService,
              private route: ActivatedRoute) {}

  @Input() userTrainings: EmployeeTraining[];

  params;
  employeeName;
  employeeTrainings: EmployeeTraining[];
  postTrainings: PostUserTraining;
  experiences: UserExperience[];
  experienceToAdd: UserExperience;
  subscription;

  ngOnInit() {

       this.subscription = this.route.paramMap.subscribe(params => {
        this.id = parseInt(params.get('employeeId'), 10);
        this.userService.getUserById(this.id).subscribe(res => {
          this.user = res;
          this.userService.getUserEmployment(this.user.accountId, this.user.userId).subscribe(userExp => {
            this.experiences = userExp;
          });
        });
      });

       this.subscription.unsubscribe();
}

  addEmployment() {
   this.experienceToAdd = {
     // replace this with user input
    AccountId: 1,
    UserId: 2,
    WorkTitle: 'Lab Intern',
    WorkDescription: 'Conducted experiments on the effects of chemicals coming in contact',
    WorkStartDate: new Date('1978-12-20T00:00:00'),
    WorkEndDate: new Date('2012-12-23T00:00:00')
   };
   this.userService.addEmployment(this.experienceToAdd).subscribe(res => res);
 }
}
