import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { LocalStorageService } from '../../services/storage.service';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validator,
  Validators,
} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/User';

@Component({
  selector: 'app-register-button',
  templateUrl: './register-button.component.html',
  styleUrls: ['./register-button.component.css'],
})
export class RegisterButtonComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private authorization: AuthService,
    private storageService: LocalStorageService,
    private router: Router
  ) {}
  user: User;
  isLoggedIn = false;
  loginPressed = false;
  loginButtonVisible = true;

  LoginForm = this.fb.group({
    userName: ['', Validators.required],
    password: ['', Validators.required],
  });

  showLogin() {
    this.loginPressed = true;
    this.loginButtonVisible = false;
  }

  login() {
    this.isLoggedIn = true;
  }

  onSubmit() {
    console.log( this.LoginForm.controls.userName.value,
      + this.LoginForm.controls.password.value);
    this.authorization
      .logIn(
        this.LoginForm.controls.userName.value,
        this.LoginForm.controls.password.value
      )
      .subscribe((res) => {
        this.user = res;
        console.log(`user ID: ${this.user.userId}`);
        // this.storageService.storeOnLocalStorage(this.user.userId);
        setTimeout(() => {this.storageService.storeOnLocalStorage(this.user.userId); }, 1000);
        this.router.navigate(['/general']);
      });
  }
  ngOnInit() {}
}
