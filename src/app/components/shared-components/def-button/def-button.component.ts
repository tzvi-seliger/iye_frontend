import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-def-button',
  templateUrl: './def-button.component.html',
  styleUrls: ['./def-button.component.scss']
})
export class DefButtonComponent implements OnInit {
@Input() buttonText: string;
  constructor() { }

  ngOnInit(): void {
  }

}
