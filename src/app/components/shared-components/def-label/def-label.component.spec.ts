import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefLabelComponent } from './def-label.component';

describe('DefLabelComponent', () => {
  let component: DefLabelComponent;
  let fixture: ComponentFixture<DefLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
