import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefTextInputComponent } from './def-text-input.component';

describe('DefTextInputComponent', () => {
  let component: DefTextInputComponent;
  let fixture: ComponentFixture<DefTextInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefTextInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefTextInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
