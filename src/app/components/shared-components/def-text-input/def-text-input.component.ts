import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-def-text-input',
  templateUrl: './def-text-input.component.html',
  styleUrls: ['./def-text-input.component.scss']
})
export class DefTextInputComponent implements OnInit {
@Input() labelText: string;
@Input() fControl: string;
  constructor() { }

  ngOnInit(): void {
  }

}
