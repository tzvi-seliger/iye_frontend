import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { PostUser } from '../../models/PostUser';
import { AuthService } from '../../services/auth.service';
import { LocalStorageService } from '../../services/storage.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss'],
})

export class SignupFormComponent implements OnInit {
  user: PostUser;
  authUser: any;
  isLoggedIn: boolean;

  accountIdLabel = 'Account Id';
  userTypeLabel = 'User Type';
  userNameLabel = 'Username';
  passwordStringLabel = 'Password';
  saltLabel = 'Salt';
  userEmailAddressLabel = 'Email';
  userFirstNameLabel = 'First Name';
  userLastNameLabel = 'Last Name';
  userPhoneNumberLabel = 'Phone Number';

  buttonText = 'Sign Up';

  signupForm = new FormGroup({
    accountId: new FormControl(''),
    userType: new FormControl(''),
    userName: new FormControl(''),
    passwordString: new FormControl(''),
    salt: new FormControl(''),
    userEmailAddress: new FormControl(''),
    userFirstName: new FormControl(''),
    userLastName: new FormControl(''),
    userPhoneNumber: new FormControl(''),
  });

  constructor(private userService: UserService,
              private router: Router,
              private authorization: AuthService,
              private storageService: LocalStorageService){}
  ngOnInit() {}

  AddUser() {
    this.user = {
      accountId: this.signupForm.value.accountId,
      userType: this.signupForm.value.userType,
      userName: this.signupForm.value.userName,
      passwordString: this.signupForm.value.passwordString,
      salt: this.signupForm.value.salt,
      userEmailAddress: this.signupForm.value.userEmailAddress,
      userFirstName: this.signupForm.value.userFirstName,
      userLastName: this.signupForm.value.userLastName,
      userPhoneNumber: this.signupForm.value.userPhoneNumber
    };

    this.userService.addUser(this.user).subscribe();
    setTimeout(() => {
      this.authorization.logIn(
        this.user.userName,
        this.user.passwordString
      ).subscribe(user => {
        this.authUser = user;
        console.log(this.user);
        this.isLoggedIn = this.user.userName === this.authUser.userName;
        console.log(this.isLoggedIn);
        this.storageService.storeOnLocalStorage(this.authUser.userId);
        this.router.navigate(['/general']);
      });
    }, 100);
  }
}
