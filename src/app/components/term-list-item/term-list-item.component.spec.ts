import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermListItemComponent } from './term-list-item.component';

describe('TermListItemComponent', () => {
  let component: TermListItemComponent;
  let fixture: ComponentFixture<TermListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
