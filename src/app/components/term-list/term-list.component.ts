import { Component, OnInit } from '@angular/core';
import { Definition } from '../../models/definition';
import { DefinitionsService } from '../../services/definitions.service';

@Component({
  selector: 'app-term-list',
  templateUrl: './term-list.component.html',
  styleUrls: ['./term-list.component.scss']
})
export class TermListComponent implements OnInit {

  definitions: Definition[];
  definitionToAdd: Definition;
  definitionToUpdate: Definition;
  isInUpdateMode: number;

  constructor(private definitionsService: DefinitionsService) {}

  ngOnInit(): void {
    this.definitionsService.getDefinitions().subscribe(res => {
      this.definitions = res;
    });
  }

  retrieveMatches(term: string): void {
    this.definitionsService.getDefinition(term).subscribe(res => {
      this.definitions = res;
    });
  }

  addDefinition(termarg: string, defarg: string): void {
    console.log(termarg + defarg);

    this.definitionToAdd = {
      term: termarg,
      definition: defarg
    };

    this.definitionsService
        .addDefinition(this.definitionToAdd)
        .subscribe(def => {
          this.definitionToAdd = def;
        });
  }

  updateDefinition(termarg: string, defarg: string) {
    this.definitionToUpdate = {
      term: termarg,
      definition: defarg
    };
    console.log(this.definitionToUpdate);
    this.definitionsService.updateDefinition(this.definitionToUpdate).subscribe(res => this.definitionToUpdate = res);
  }


  changeToUpdateMode(idx: number): void {
    this.isInUpdateMode = idx;
  }

  IsActiveTerm(idx: number): boolean {
    return this.isInUpdateMode === idx;
  }
}
