import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { TrainingsService } from '../../services/trainings.service';
import { Training } from '../../models/Training';
import { ActivatedRoute } from '@angular/router';
import { TrainingFile } from '../../models/TrainingFile';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
})

export class TrainingComponent implements OnInit {
  current: Training;
  trainingsCollection;
  files: TrainingFile[];
  id: number;

  public progress: number;
  public message: string;
  public formData = new FormData();

  @Output() public UploadFinished = new EventEmitter();

  constructor(
    private trainings: TrainingsService,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {}
  ngOnInit() {
    // this.trainings.getTrainings().subscribe((res) => {
    //   this.trainingsCollection = res;
    //   this.route.paramMap.subscribe((params) => {
    //     this.id = parseInt(params.get('trainingId'), 10);
    //     this.trainings.getFilesForTraining(this.id).subscribe((res2) => {
    //       this.files = res2;
    //     });
    //     this.current = this.trainingsCollection.filter(
    //       (training) => training.trainingID === this.id
    //     )[0];
    //   });
    // });
    this.trainings.getTrainings().subscribe((res) => {
      this.trainingsCollection = res;
    });
  }
  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }

    const fileToUpload = files[0] as File;
    this.formData.append('file', fileToUpload, fileToUpload.name);
  }

  public onCreate() {
    this.http
    .post('https://localhost:44386/api/trainings/addfile', this.formData, {
      reportProgress: true,
      observe: 'events',
    })
    .subscribe((event) => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round((100 * event.loaded) / event.total);
      } else if (event.type === HttpEventType.Response) {
        this.message = 'Upload success.';
        this.UploadFinished.emit(event.body);
      }
    });
  }
}
