import { Component, OnInit } from '@angular/core';
import { TrainingsService } from '../../services/trainings.service';
import { Training } from '../../models/Training';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss']
})


export class TrainingsComponent implements OnInit {
  importedtrainings: Training[];

  constructor(private trainings: TrainingsService) { }

  ngOnInit() {
    this.trainings.getTrainings().subscribe((res) => {
      this.importedtrainings = res;
      console.log(res);
    });
  }
}
