import { ArticleBase } from './ArticleBase';

export class Article extends ArticleBase {
    content: string;
    rating: number;
    author: string;
    level: string;
    subject: string;
}
