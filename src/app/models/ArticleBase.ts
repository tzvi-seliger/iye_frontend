export class ArticleBase {
    id: number;
    title: string;
    description: string;
}
