export class ArticleFilters {
    Ratings: number[];
    Subjects: string[];
    Levels: string[];
    Authors: string[];
}
