export class ArticlePost {
    title: string;
    description: string;
    content: string;
    rating: number;
    author: string;
    level: string;
    subject: string;
}
