export class EmployeeTraining {
    userId: number;
    trainingName: string;
    trainingStatus: string;
    userFirstName: string;
    userLastName: string;
}
