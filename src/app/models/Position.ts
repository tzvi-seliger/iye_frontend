export class Position {
    accountId: number;
    salaryMin: number;
    salaryMax: number;
    location: string;
    isRemote: boolean;
    experienceRequired: number;
    title: string;
    description: string;
  }
