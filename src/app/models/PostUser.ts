export class PostUser {
    accountId: number;
    userType: string;
    userName: string;
    passwordString: string;
    salt: string;
    userEmailAddress: string;
    userFirstName: string;
    userLastName: string;
    userPhoneNumber: string;
  }
