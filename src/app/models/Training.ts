export class Training {
    trainingId: number;
    accountId: number;
    trainingName: string;
    trainingDescription: string;
}
