export class TrainingFile {
  trainingFileID: number;
  trainingFileOrderNo: number;
  trainingFileDescription: string;
  trainingFilePath: string;
}
