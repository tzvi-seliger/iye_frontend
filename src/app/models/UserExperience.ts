export class UserExperience {
    AccountId: number;
    UserId: number;
    WorkTitle: string;
    WorkDescription: string;
    WorkStartDate: Date;
    WorkEndDate: Date;
}
