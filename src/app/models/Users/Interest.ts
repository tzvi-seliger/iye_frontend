export class Interest {
    userId: number;
    interest: string;
    description: string;
}
