export class Skill {
    userId: number;
    skill: string;
    description: string;
}
