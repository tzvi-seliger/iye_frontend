import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, range } from 'rxjs';
import { Interest } from '../../models/Users/Interest';

@Injectable({
    providedIn: 'root',
})
export class InterestsService {
    constructor( private http: HttpClient ) { }

    GetInterests(id: number): Observable<Interest[]> {
       return this.http.get<Interest[]>(`https://localhost:44386/api/users/interests/${id}`);
    }

    editInterest(id: string, interest: Interest): Observable<Interest> {
        return this.http.put<Interest>(`https://localhost:44386/api/users/interests/${id}`, interest);
    }

    deleteInterest(id: string): Observable<Interest> {
        return this.http.delete<Interest>(`https://localhost:44386/api/users/interests/${id}`);
    }

    addInterest(interest: Interest): Observable<Interest> {
        return this.http.post<Interest>(`https://localhost:44386/api/users/interests`, interest);
    }
}
