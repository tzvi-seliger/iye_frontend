import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, range } from 'rxjs';
import { Skill } from '../../models/Users/Skills';

@Injectable({
    providedIn: 'root',
})
export class SkillsService {
    constructor( private http: HttpClient ) { }

    GetSkills(id: number): Observable<Skill[]> {
       return this.http.get<Skill[]>(`https://localhost:44386/api/users/${id}/skills`);
    }

    editSkill(id: string, skill: Skill): Observable<Skill> {
        return this.http.put<Skill>(`https://localhost:44386/api/users/skills/${id}`, skill);
    }

    deleteSkill(id: string): Observable<Skill> {
        return this.http.delete<Skill>(`https://localhost:44386/api/users/skills/${id}`);
    }

    addSkill(skill: Skill): Observable<Skill> {
        return this.http.post<Skill>(`https://localhost:44386/api/users/skills`, skill);
    }
}
