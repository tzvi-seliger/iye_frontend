import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, range } from 'rxjs';
import { Training } from '../../models/Training';

@Injectable({
    providedIn: 'root',
})
export class UserTrainingsService {
    constructor( private http: HttpClient ) { }

    GetUserAuthoredTrainings(id: number): Observable<Training[]> {
       return this.http.get<Training[]>(`https://localhost:44386/api/users/${id}/AuthoredTrainings`);
    }

    GetUserCompletedTrainings(id: number): Observable<Training[]> {
        return this.http.get<Training[]>(`https://localhost:44386/api/users/${id}/CompletedTrainings`);
     }
}
