import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, range } from 'rxjs';
import { UserExperience } from '../../models/UserExperience';

@Injectable({
    providedIn: 'root',
})
export class SkillsService {
    constructor( private http: HttpClient ) { }

    GetWorkExperiences(id: number): Observable<UserExperience[]> {
       return this.http.get<UserExperience[]>(`https://localhost:44386/api/users/${id}/experiences`);
    }

    editWorkExperience(id: string, workExperience: UserExperience): Observable<UserExperience> {
        return this.http.put<UserExperience>(`https://localhost:44386/api/users/experiences/${id}`, workExperience);
    }

    deleteWorkExperience(id: string): Observable<UserExperience> {
        return this.http.delete<UserExperience>(`https://localhost:44386/api/users/experiences/${id}`);
    }

    addWorkExperience(workExperience: UserExperience): Observable<UserExperience> {
        return this.http.post<UserExperience>(`https://localhost:44386/api/users/experiences`, workExperience);
    }
}
