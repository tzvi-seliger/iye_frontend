import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, range} from 'rxjs';
import { Article } from '../models/Article';
import { ArticleBase } from '../models/ArticleBase';
import { ArticlePost } from '../models/ArticlePost';
import { ArticleFilters } from '../models/ArticleFilters';

@Injectable({
    providedIn: 'root',
})
export class ArticlesService {
    constructor( private http: HttpClient ) { }

    addArticle(article: ArticlePost): Observable<ArticlePost> {
       return this.http.post<ArticlePost>('https://localhost:44386/api/articles', article);
    }
    getArticleById(id: number): Observable<Article> {
        return this.http.get<Article>(`https://localhost:44386/api/articles/${id}`);
    }

    getArticleTitles(): Observable<ArticleBase[]> {
        return this.http.get<ArticleBase[]>(`https://localhost:44386/api/articles`);
    }

    getArticleTitlesFiltered(articleFilters: {}): Observable<ArticleBase[]> {
      return this.http.post<ArticleBase[]>(`https://localhost:44386/api/articles/FilteredArticles`, articleFilters);
    }

    getArticleTitlesByKeyword(keyword: string): Observable<ArticleBase[]> {
        return this.http.get<ArticleBase[]>(`https://localhost:44386/api/articles/ArticleByKeyword/${keyword}`);
    }

    editArticle(article: Article): Observable<Article> {
        return this.http.put<Article>(`https://localhost:44386/api/articles`, article);
    }

    deleteArticle(id: string): Observable<Article> {
        return this.http.delete<Article>(`https://localhost:44386/api/articles/${id}`);
    }

    getAuthors(): Observable<string[]>{
      return this.http.get<string[]>('https://localhost:44386/api/articles/authors');
    }
    getSubjects(): Observable<string[]> {
      return this.http.get<string[]>('https://localhost:44386/api/articles/subjects');
    }

    getRatings(): Observable<string[]> {
      return this.http.get<string[]>('https://localhost:44386/api/articles/ratings');
    }

    getLevels(): Observable<string[]> {
      return this.http.get<string[]>('https://localhost:44386/api/articles/levels');
    }
}
