import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Definition } from '../models/definition';

@Injectable({
  providedIn: 'root'
})
export class DefinitionsService {

  constructor( private http: HttpClient ) { }

  getDefinitions(): Observable<Definition[]> {
      return this.http.get<Definition[]>('https://localhost:44386/api/definitions');
  }

  getDefinition(term: string): Observable<Definition[]> {
    return this.http.get<Definition[]>(`https://localhost:44386/api/definitions/TermDefinition/${term}`);
  }

  addDefinition(definition: Definition): Observable<Definition> {
    return this.http.post<Definition>('https://localhost:44386/api/definitions', definition);
  }

  updateDefinition(termDefinition: Definition): Observable<any> {
   return this.http.put<Observable<any>>(`https://localhost:44386/api/definitions`, termDefinition);
  }
}
