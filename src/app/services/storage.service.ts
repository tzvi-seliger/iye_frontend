import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

const USER_ID = 'user_ID';

@Injectable()
export class LocalStorageService {
     constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }
     public storeOnLocalStorage(userId: number): void {
          this.storage.set(USER_ID, userId);
     }

     public retrieveUserId(): number {
         return this.storage.get(USER_ID);
     }
}
