import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, range} from 'rxjs';
import {map, filter} from 'rxjs/operators';
import { Training } from '../models/Training';
import { TrainingFile } from '../models/TrainingFile';
import { EmployeeTraining } from '../models/EmployeeTraining';
import { PostUserTraining } from '../models/PostUserTraining';

@Injectable({
    providedIn: 'root',
})
export class TrainingsService {
    constructor( private http: HttpClient ) { }

    getTrainings(): Observable<Training[]> {
        return this.http.get<Training[]>('https://localhost:44386/api/trainings');
    }

    getFilesForTraining(id: number): Observable<TrainingFile[]> {
        return this.http.get<TrainingFile[]>(`https://localhost:44386/api/trainings/trainingFiles/${id}`);
    }

    getEmployeeTrainings(userName: string): Observable<EmployeeTraining[]> {
        return this.http.get<EmployeeTraining[]>(`https://localhost:44386/api/trainings/GetEmployeeTrainings/${userName}`);
    }

    getEmployeeTrainingsById(id: number): Observable<EmployeeTraining[]> {
        return this.http.get<EmployeeTraining[]>(`https://localhost:44386/api/trainings/GetEmployeeTrainingsById/${id}`);
    }

    AddUserTrainings(userTraining: PostUserTraining) {
        this.http.post(`https://localhost:44386/api/trainings/AddUserTraining`, userTraining);
    }
}
