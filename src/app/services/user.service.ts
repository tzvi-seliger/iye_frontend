import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, range } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { User } from '../models/User';
import { PostUser } from '../models/PostUser';
import { UserExperience } from '../models/UserExperience';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}
  getUser(username: any, password: any): Observable<User> {
    return this.http.get<User>(
      `https://localhost:44386/api/users/${username}/${password}`
    );
  }

  getImage(): Observable<any> {
    return this.http.get(`https://localhost:44386/api/user/getImage`);
  }

  addUser(user: PostUser): Observable<PostUser> {
    return this.http.post<PostUser>(`https://localhost:44386/api/users`, user);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`https://localhost:44386/api/users/${id}`);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('https://localhost:44386/api/users');
  }

  addEmployment(user: UserExperience): Observable<UserExperience> {
    return this.http.post<UserExperience>(`https://localhost:44386/api/users/employment`, user);
  }

  getUserEmployment(accountId: number, userId: number): Observable<UserExperience[]> {
    return this.http.get<UserExperience[]>(`https://localhost:44386/api/users/employment/${accountId}/${userId}`);
  }
}
